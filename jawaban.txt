1. Buat Database :
    create database myshop;

2. Membuat Table di dalam Database :
    
    Table Users:
    create table users (
        id int(8) auto_increment,
        name varchar(255),
        email varchar(255),
        password varchar(255),
        primary key(id)
    );

    Table Categories:
    create table categories (
        id int(8) auto_increment,
        name varchar(255),
        primary key(id)
    );

    Table Items:
    create table categories (
        id int(8) auto_increment,
        name varchar(255),
        description varchar(255),
        price key(id),
        stock key(id),
        primary key(id)
    );

3. Memasukan data pada Table
    Table Users:
        insert into users (name, email, password) values ("John Doe","john@doe.com","john123"),  ("Jane Doe","jane@doe.com","jenita123");
    Table Categories:
        insert into categories (name) values ("gadget"),("cloth"),("men"),("women"),("branded");
    Table Items:
        insert into items (name, description, price, stock, category_id) values ("Sumsang b50","hape keren dari merek sumsang", 4000000, 100, 1),("Uniklooh"," 	baju keren dari brand ternama",500000,50,2),("IMHO Watch"," 	jam tangan anak yang jujur banget",2000000,10,1);

4. Mengambil data dari Database
    a. mengambil data users
        select name, email from users;
    b. mengambil data items
        > select * from items where price > 1000000
        > select * from items where name like 'uniklo%';
    c. menampilkan data items join dengan kategori
        select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.id = categories.id;

5. Mengubah data dari Database
    updata items set price = 2500000 where id = 1;